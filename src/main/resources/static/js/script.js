moment.locale('ru');

let end_of_list = false;
let searching = false;
let query = "";
let category = "";
let seller_id = 0;
let page = 0;

let new_items;

const url_items = "/api/items";
const url_info = "/api/info";
const url_newitems = "/api/newitems";

const spinner = $('#spinner');
const showNew = $('#showNew');

let enter_time = Math.round(Date.now() / 1000);

let queryDict = {};

let categories = new Map();
categories.set("BLASTERS", "Привода");
categories.set("GEAR", "Экипировка и Снаряжение");
categories.set("PARTS", "Аксессуары и Запчасти");
categories.set("MIXED", null);

location.search.substr(1).split("&").forEach(function (item) {
    queryDict[item.split("=")[0]] = item.split("=")[1]
});

if (queryDict.query != null && queryDict.query !== "") {
    query = decodeURIComponent(queryDict.query).replace(/\+/g, ' ');
    searching = true;
    document.getElementById('search_query').value = query;
}

if (queryDict.category != null && queryDict.category !== "") {
    category = queryDict.category;
    searching = true;
    document.querySelector('option[value=' + category + ']').selected = true
}

if (queryDict.seller != null && queryDict.seller !== 0) {
    seller_id = queryDict.seller;
    searching = true;
    document.getElementById('search_seller').value = seller_id;
}

loadPage();
fillInfo();


function fillInfo() {
    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=UTF-8',
        url: url_info,
        success: function (result) {
            console.log(result);
            document.getElementById('bd_count').innerHTML = result.items;
            document.getElementById('version').innerHTML = result.version;
            document.getElementById('server_problem').remove();
        }
    });
}

function loadPage() {
    let url = url_items;
    let data = {page: page, query: query, category: category, seller: seller_id, start_time: enter_time};

    console.log("loading page " + page + " from url " + url + " with data");
    console.log(data);

    spinner.removeClass("hide");

    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=UTF-8',
        url: url,
        data: data,
        success: function (results) {
            console.log(results);

            addItems(results.content, true);

            if (results.first && results.content.length > 0) {
                enter_time = results.content[0].timestamp;
            }

            spinner.addClass("hide");

            if (results.last) {
                end_of_list = true;
            }

            if (query !== "") {
                $('#itemslist').mark(query);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
}

function addItems(items, bottom) {
    let itemslist = $('#itemslist');

    items.forEach(function (item) {

        let cat = categories.get(item.vkAlbum.category);

        let el = $('#row_template').clone();
        el.removeClass("hide");

        el.find('#photo_link').attr('href', 'http://vk.com/' + item.url);
        el.find('#photo_image').attr('src', item.photo_src);
        el.find('#user_link').attr('href', "https://vk.com/id" + item.seller.vkId).text(item.seller.name);
        el.find('#user_link_text').text("https://vk.com/id" + item.seller.vkId);
        el.find('#public_link').attr('href', "https://vk.com/public" + item.vkGroup.vkId).text(item.vkGroup.name);
        el.find('#public_link_text').text("https://vk.com/public" + item.vkGroup.vkId);
        el.find('#item_category').append(cat);
        el.find('#item_added').append(moment(item.timestamp * 1000).calendar());
        el.find('#item_desc').append(item.info.replace(/(?:\r\n|\r|\n)/g, '<br />'));

        let duplicates = item.duplicates;
        if (duplicates.length !== 0) {
            el.find('#item_duplicates').removeClass("hide");
            let list = el.find("#item_duplicates_list");

            duplicates.forEach(function (duplicate) {
                list.append("<li><a href='https://vk.com/" + duplicate.url + "' target='_blank' >" + duplicate.vkGroup.name + "</a></li>")
            });
        }

        if (cat == null) {
            el.find('#item_category').addClass("hide");
        }

        if (bottom === true) {
            itemslist.append(el);
            itemslist.append("<hr/>");
        } else {
            itemslist.prepend("<hr/>");
            itemslist.prepend(el);
        }
    });
}

window.onscroll = function () {
    scrollFunction();

    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        if (!end_of_list && spinner.hasClass("hide")) {
            page++;
            loadPage();
        }
    }
};

function scrollFunction() {
    if (document.body.scrollTop > 350 || document.documentElement.scrollTop > 350) {
        document.getElementById("toTop").style.display = "block";
    } else {
        document.getElementById("toTop").style.display = "none";
    }
}

/**
 * Скролл к якорю
 */
function scrollToTop() {
    $("html, body").animate({scrollTop: 350}, "slow");
}

/**
 * показать новые элементы
 */
function loadNewItems() {
    scrollToTop();
    //убрать кнопку
    showNew.addClass("hide");
    //добавить новые значения в начало списка
    addItems(new_items, false);

    enter_time = new_items[0].timestamp;

    // очистить список
    new_items = {};
}

/**
 * Раз в 2 минуты чекать появление новых объявлений
 */
setInterval(function () {
    let data = {page: page, query: query, category: category, seller: seller_id, start_time: enter_time};

    console.log("checking new items from url " + url_newitems + " for requests:");
    console.log(data);

    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=UTF-8',
        url: url_newitems,
        data: data,
        success: function (results) {
            let length = results.length;
            if (length > 0) {
                new_items = results;
                showNew.removeClass("hide");
                showNew.text("Новых лотов: " + length);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
}, 1000 * 60 * 2);
