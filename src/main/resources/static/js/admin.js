moment.locale('ru_RU');

let password = "ты даже не пытался";

const login_form = $('#login_form');
const login_button = $('#login_button');
const password_field = $('#password_field');
const admin_panel = $('#admin_panel');
const table_container = $("#table_container");
const add_album_by_id = $("#add_album_by_id");
const owner_id = $("#owner_id");
const album_id = $("#album_id");
const category_by_id = $("#category_by_id");
const album_link_by_id = $("#album_link_by_id");
const button_init = $("#button_init");
const button_parser = $("#button_parser");
const button_gc = $("#button_gc");
const button_cover = $("#button_cover");

const tr_group_template_clone = $("#tr_group_template").clone();
const tr_album_template_clone = $("#tr_album_template").clone();

login_button.on("click", function () {
    password = password_field.val();
    login_form.fadeOut(300);
    admin_panel.delay(300).fadeIn();
    loadAlbums();
});

button_init.on("click", function () {
    init();
});

button_parser.on("click", function () {
    $.ajax({
        type: "POST",
        url: "/api/admin/runparser",
        data: {password: password},
        success: function (results) {
            console.log(results);

            loadAlbums();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
});

button_gc.on("click", function () {
    $.ajax({
        type: "POST",
        url: "/api/admin/runparser",
        data: {password: password},
        success: function (results) {
            console.log(results);

            loadAlbums();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
});

button_cover.on("click", function () {
    $.ajax({
        type: "POST",
        url: "/api/admin/updatecover",
        data: {password: password},
        success: function (results) {
            console.log(results);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
});

add_album_by_id.on("click", function () {
    let link = album_link_by_id.val();
    let data;
    if (link !== "") {
        link = link.replace("https://vk.com/album-", "");
        data = {
            owner_id: parseInt(link.split("_")[0]),
            album_id: parseInt(link.split("_")[1]),
            password: password,
            category: category_by_id.val()
        };
    } else {
        data = {password: password, owner_id: owner_id.val(), album_id: album_id.val(), category: category_by_id.val()};
    }

    $.ajax({
        type: "POST",
        url: "/api/admin/album",
        data: data,
        success: function (results) {
            console.log(results);

            loadAlbums();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });

    album_link_by_id.val('');
});

login_form.on("keypress", function (e) {
    if (e.keyCode === 13) {
        login_button.trigger("click");
        return false;
    }
});

function loadAlbums() {
    console.log("Получаем альбомы с паролем " + password + " от " + "/api/admin/groups");

    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=UTF-8',
        url: "/api/admin/groups",
        data: {password: password},
        success: function (results) {
            console.log("Ответ: " + results);

            //clear children
            table_container.find("tr:gt(0)").remove();

            results.forEach(function (group) {
                let el = tr_group_template_clone.clone();
                el.removeClass("hide");

                let icon;
                switch (group.status) {
                    case "UNKNOWN":
                        icon = "help-circle";
                        break;
                    case "FAIL":
                        icon = "alert-circle";
                        break;
                    case "OK":
                        icon = "check";
                        break;
                }

                el.find('#group_status_icon').html("<i data-feather=\"" + icon + "\"></i>");
                el.find('#group_link').attr('href', "https://vk.com/public" + (group.vkId)).text(group.vkId);
                el.find('#group_name').text(group.name);
                el.find(".action_group_delete").click(function () {
                    deleteGroup(group.vkId);
                });
                table_container.append(el);

                group.albums.forEach(function (album) {
                    let el = tr_album_template_clone.clone();
                    el.removeClass("hide");

                    let icon;
                    switch (album.status) {
                        case "UNKNOWN":
                            icon = "help-circle";
                            break;
                        case "FAIL":
                            icon = "alert-circle";
                            break;
                        case "OK":
                            icon = "check";
                            break;
                    }

                    el.find('#album_status_icon').html("<i data-feather=\"" + icon + "\"></i>");
                    el.find('#album_link').attr('href', "https://vk.com/album" + (-group.vkId) + "_" + album.vkId).text(album.vkId);
                    el.find('#album_category').append(album.category);
                    el.find('#album_name').text(album.name);
                    el.find(".action_album_delete").click(function () {
                        deleteAlbum(album.vkId);
                    });

                    table_container.append(el);
                });
            });

            feather.replace();

        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
}

const GUNS = "BLASTERS";
const GEAR = "GEAR";
const PART = "PARTS";
const MIXE = "MIXED";

function init() {
    let asd = [
        {category: GUNS, password: password, owner_id: 158636, album_id:101939607},
        {category: GEAR, password: password, owner_id: 158636, album_id:101939688},
        {category: MIXE, password: password, owner_id: 11020514, album_id:240761882},
        {category: GUNS, password: password, owner_id: 11571122, album_id:229924509},
        {category: GEAR, password: password, owner_id: 11571122, album_id:229924632},
        {category: PART, password: password, owner_id: 11571122, album_id:229924703},
        {category: PART, password: password, owner_id: 13212026, album_id:259241640},
        {category: GEAR, password: password, owner_id: 13212026, album_id:259241656},
        {category: GUNS, password: password, owner_id: 13212026, album_id:259241672},
        {category: GUNS, password: password, owner_id: 27239071, album_id:255071521},
        {category: PART, password: password, owner_id: 27239071, album_id:255071555},
        {category: GEAR, password: password, owner_id: 27239071, album_id:255071572},
        {category: MIXE, password: password, owner_id: 49059245, album_id:235100157},
        {category: GEAR, password: password, owner_id: 68239102, album_id:192518921},
        {category: GUNS, password: password, owner_id: 76629546, album_id:203426857},
        {category: PART, password: password, owner_id: 76629546, album_id:203426935},
        {category: GEAR, password: password, owner_id: 76629546, album_id:203426992},
        {category: GEAR, password: password, owner_id: 85601282, album_id:210099632},
        {category: MIXE, password: password, owner_id: 85601282, album_id:210099652},
        {category: GEAR, password: password, owner_id: 85601282, album_id:210099948},
        {category: GUNS, password: password, owner_id: 90157786, album_id:244825250},
        {category: GEAR, password: password, owner_id: 90157786, album_id:244825308},
        {category: PART, password: password, owner_id: 90157786, album_id:244825356},
        {category: GUNS, password: password, owner_id: 97006205, album_id:217566735},
        {category: PART, password: password, owner_id: 97006205, album_id:217566811},
        {category: GEAR, password: password, owner_id: 97006205, album_id:236163598},
        {category: GUNS, password: password, owner_id: 97278600, album_id:221381445},
        {category: PART, password: password, owner_id: 97278600, album_id:221381462},
        {category: GEAR, password: password, owner_id: 97278600, album_id:221381469},
        {category: PART, password: password, owner_id: 98851139, album_id:250235284},
        {category: GUNS, password: password, owner_id: 98851139, album_id:250235426},
        {category: GEAR, password: password, owner_id: 98851139, album_id:250235533},
        {category: MIXE, password: password, owner_id: 101005011, album_id:241685721},
        {category: GEAR, password: password, owner_id: 102504160, album_id:239128209},
        {category: MIXE, password: password, owner_id: 103349796, album_id:246577746},
        {category: MIXE, password: password, owner_id: 107247182, album_id:223784774},
        {category: GUNS, password: password, owner_id: 148842595, album_id:245051741},
        {category: GUNS, password: password, owner_id: 148842595, album_id:245051921},
        {category: GEAR, password: password, owner_id: 148842595, album_id:245052181},
        {category: GEAR, password: password, owner_id: 148842595, album_id:245052294},
        {category: GUNS, password: password, owner_id: 148842595, album_id:245052962},
        {category: GUNS, password: password, owner_id: 148842595, album_id:245053435},
        {category: GUNS, password: password, owner_id: 148842595, album_id:245053914},
        {category: PART, password: password, owner_id: 148842595, album_id:245054108},
        {category: GEAR, password: password, owner_id: 148842595, album_id:245056422},
        {category: PART, password: password, owner_id: 148842595, album_id:245056571},
        {category: GEAR, password: password, owner_id: 148842595, album_id:245056823},
        {category: PART, password: password, owner_id: 148842595, album_id:245056949},
        {category: PART, password: password, owner_id: 148842595, album_id:245057284},
        {category: GEAR, password: password, owner_id: 166126912, album_id:253462227},
        {category: GEAR, password: password, owner_id: 166126912, album_id:253462701},
        {category: GUNS, password: password, owner_id: 166126912, album_id:253474469},
        {category: PART, password: password, owner_id: 166126912, album_id:253476356},
        {category: GEAR, password: password, owner_id: 166126912, album_id:253528546},
        {category: GEAR, password: password, owner_id: 166126912, album_id:253944709},
        {category: GEAR, password: password, owner_id: 166126912, album_id:253945518},
    ];


    asd.forEach(function (value, index) {
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "/api/admin/album",
                data: value,
                success: function (results) {
                    console.log(results);

                    loadAlbums();
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
                }
            });

        }, 1000 * index);
    })
}

function deleteGroup(id) {
    $.ajax({
        type: "POST",
        url: "/api/admin/groupdelete",
        data: {password: password, group_id: id},
        success: function (results) {
            console.log(results);
            loadAlbums();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });

}

function deleteAlbum(id) {
    $.ajax({
        type: "POST",
        url: "/api/admin/albumdelete",
        data: {password: password, album_id: id},
        success: function (results) {
            console.log(results);
            loadAlbums();
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log("Error: " + errorThrown + xhr.status + xhr.responseText);
        }
    });
}