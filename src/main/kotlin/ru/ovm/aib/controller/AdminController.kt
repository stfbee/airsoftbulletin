package ru.ovm.aib.controller

import com.vk.api.sdk.exceptions.ApiException
import com.vk.api.sdk.exceptions.ClientException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import ru.ovm.aib.App
import ru.ovm.aib.ScheduledTasks
import ru.ovm.aib.db.pojo.Category
import ru.ovm.aib.db.pojo.Status
import ru.ovm.aib.db.pojo.VkAlbum
import ru.ovm.aib.db.pojo.VkGroup
import ru.ovm.aib.db.repository.AlbumRepository
import ru.ovm.aib.db.repository.GroupRepository
import ru.ovm.aib.db.repository.ItemRepository
import ru.ovm.aib.db.returns.Result
import java.awt.FontFormatException
import java.io.IOException

@RestController
@RequestMapping("/api/admin/")
class AdminController @Autowired
constructor(private val itemRepository: ItemRepository, private val albumRepository: AlbumRepository, private val groupRepository: GroupRepository, private val tasks: ScheduledTasks) {


    //   #####  ####### #######
    //  #     # #          #
    //  #       #          #
    //  #  #### #####      #
    //  #     # #          #
    //  #     # #          #
    //   #####  #######    #

    @RequestMapping(value = ["/groups"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun getGroups(@RequestParam password: String): Any {
        return if (App.admin != password) Result.NOPASSWORD else groupRepository.findAllByDeletedIs(false)
    }

    @RequestMapping(value = ["/albums"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun getAlbums(@RequestParam password: String): Any {
        return if (App.admin != password) Result.NOPASSWORD else albumRepository.findAllByDeletedIs(false)
    }


    //  ######  ####### #       ####### ####### #######
    //  #     # #       #       #          #    #
    //  #     # #       #       #          #    #
    //  #     # #####   #       #####      #    #####
    //  #     # #       #       #          #    #
    //  #     # #       #       #          #    #
    //  ######  ####### ####### #######    #    #######

    @RequestMapping(value = ["/albumdelete"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun deleteAlbum(@RequestParam password: String,
                    @RequestParam album_id: Int?): Any {

        if (App.admin != password) return Result.NOPASSWORD
        if (album_id == null) return Result.NOALBUM

        val albumOptional = albumRepository.findById(album_id)
        if (!albumOptional.isPresent) {
            return Result.NOALBUM
        }
        val vkAlbum = albumOptional.get()
        App.LOG.info("Удаляем альбом " + vkAlbum.vkId + "...")
        itemRepository.findByVkAlbum(vkAlbum).forEach { item ->
            item.deleted = true
            itemRepository.save(item)
        }
        App.LOG.info("Готово, теперь сам альбом")
        vkAlbum.deleted = true
        albumRepository.save(vkAlbum)
        App.LOG.info("Альбом с фотками удален")

        return Result.SUCCESS
    }

    @RequestMapping(value = ["/groupdelete"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun deleteGroup(@RequestParam password: String,
                    @RequestParam group_id: Int?): Any {

        if (App.admin != password) return Result.NOPASSWORD
        if (group_id == null) return Result.NOGROUP

        val groupOptional = groupRepository.findById(group_id)
        if (!groupOptional.isPresent) return Result.NOGROUP

        val vkGroup = groupOptional.get()
        App.LOG.info("Удаляем группу ${vkGroup.vkId}...")

        for (vkAlbum in vkGroup.albums) {
            itemRepository.findByVkAlbum(vkAlbum).forEach { item ->
                item.deleted = true
                itemRepository.save(item)
            }
            App.LOG.info("Готово, теперь сам альбом")
            vkAlbum.deleted = true
            albumRepository.save(vkAlbum)
            App.LOG.info("Альбом с фотками удален")
        }

        vkGroup.deleted = true
        groupRepository.save(vkGroup)

        App.LOG.info("группа ${vkGroup.vkId} удалена со всеми лотами")

        return Result.SUCCESS
    }


    //  ######  #######  #####  #######
    //  #     # #     # #     #    #
    //  #     # #     # #          #
    //  ######  #     #  #####     #
    //  #       #     #       #    #
    //  #       #     # #     #    #
    //  #       #######  #####     #

    @RequestMapping(value = ["/album"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun addAlbumById(
            @RequestParam album_id: Int,
            @RequestParam owner_id: Int,
            @RequestParam category: Category,
            @RequestParam password: String): Any {
        var owner = owner_id

        if (App.admin != password) return Result.NOPASSWORD

        try {
            if (owner < 0) owner = -owner

            val groupOptional = groupRepository.findById(owner)
            val vkGroup: VkGroup


            if (!groupOptional.isPresent) {
                val groupObjects = App.vk.groups().getById(App.actorU).groupId(owner.toString()).execute()
                vkGroup = VkGroup(owner, groupObjects[0].name)
                vkGroup.status = Status.UNKNOWN
                groupRepository.save(vkGroup)

                App.LOG.info("Группа добавлена: ${vkGroup.vkId} ${vkGroup.name}")
            } else {
                vkGroup = groupOptional.get()
            }

            val albumOptional = albumRepository.findById(album_id)

            val albumFullList = App.vk.photos().getAlbums(App.actorU).ownerId(-owner).albumIds(album_id).execute().items
            if (albumFullList.isEmpty()) return Result.NOALBUM

            val photoAlbumFull = albumFullList[0]

            if (!albumOptional.isPresent) {
                val vkAlbum = VkAlbum(album_id, photoAlbumFull.title, category, vkGroup)
                vkAlbum.status = Status.UNKNOWN
                albumRepository.save(vkAlbum)
                App.LOG.info("Альбом добавлен: ${vkAlbum.vkId} ${vkAlbum.name}")
            } else {
                val vkAlbum = albumOptional.get()
                vkAlbum.name = photoAlbumFull.title
                vkAlbum.category = category
                albumRepository.save(vkAlbum)
                App.LOG.info("Альбом обновлен: ${vkAlbum.vkId} ${vkAlbum.name}")
            }
        } catch (e: ApiException) {
            e.printStackTrace()
            return Result.INTERNALERROR
        } catch (e: ClientException) {
            e.printStackTrace()
            return Result.INTERNALERROR
        }

        return Result.SUCCESS
    }


    //     #     #####  ####### ### ####### #     #
    //    # #   #     #    #     #  #     # ##    #
    //   #   #  #          #     #  #     # # #   #
    //  #     # #          #     #  #     # #  #  #
    //  ####### #          #     #  #     # #   # #
    //  #     # #     #    #     #  #     # #    ##
    //  #     #  #####     #    ### ####### #     #

    @RequestMapping(value = ["/runparser"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    @Throws(InterruptedException::class)
    fun runParser(@RequestParam password: String): Any {
        if (App.admin != password) return Result.NOPASSWORD
        tasks.runParser()
        return Result.SUCCESS
    }

    @RequestMapping(value = ["/rungc"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun checkOldItems(@RequestParam password: String): Any {
        if (App.admin != password) return Result.NOPASSWORD
        tasks.checkOldItems()
        return Result.SUCCESS
    }

    @RequestMapping(value = ["/updatecover"], method = [RequestMethod.POST], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun updateCover(@RequestParam password: String): Any {
        if (App.admin != password) return Result.NOPASSWORD
        try {
            tasks.makeCover()
        } catch (e: IOException) {
            e.printStackTrace()

            return Result.INTERNALERROR
        } catch (e: FontFormatException) {
            e.printStackTrace()
            return Result.INTERNALERROR
        } catch (e: ApiException) {
            e.printStackTrace()
            return Result.INTERNALERROR
        } catch (e: ClientException) {
            e.printStackTrace()
            return Result.INTERNALERROR
        }

        return Result.SUCCESS
    }


}