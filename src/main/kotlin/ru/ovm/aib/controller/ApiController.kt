package ru.ovm.aib.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.web.bind.annotation.*
import ru.ovm.aib.db.pojo.Category
import ru.ovm.aib.db.pojo.Item
import ru.ovm.aib.db.pojo.Seller
import ru.ovm.aib.db.repository.AlbumRepository
import ru.ovm.aib.db.repository.GroupRepository
import ru.ovm.aib.db.repository.ItemRepository
import ru.ovm.aib.db.returns.Info
import ru.ovm.aib.db.returns.Result
import ru.ovm.aib.db.search.ItemSpecification
import ru.ovm.aib.db.search.SearchCriteria
import java.util.*

@RestController
@RequestMapping("/api/")
class ApiController @Autowired
constructor(private val itemRepository: ItemRepository, private val groupRepository: GroupRepository, private val albumRepository: AlbumRepository) {

    private val timestampOrderDESC = Sort.Order(Sort.Direction.DESC, "timestamp")
    private val idOrder = Sort.Order(Sort.Direction.DESC, "id")
    private val orders = Sort.by(timestampOrderDESC, idOrder)

    @RequestMapping(value = ["/info"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun getBdInfo(): Info {
        return Info(itemRepository.countActive(), groupRepository.countActive(), albumRepository.countActive())
    }

    @RequestMapping(value = ["/items"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun findItemsFor(
            @RequestParam(required = false, defaultValue = "") query: String,
            @RequestParam(required = false, defaultValue = "ALL") category: Category,
            @RequestParam(required = false) seller: Seller?,
            @RequestParam(required = false, defaultValue = "0") page: Int,
            @RequestParam(required = false, defaultValue = "50") count: Int,
            @RequestParam(required = false, defaultValue = "-1") start_time: Int,
            @RequestParam(required = false, defaultValue = "false") deleted: Boolean): Page<Item> {
        var startTime = start_time

        if (startTime == -1) {
            startTime = Math.toIntExact(Date().time / 1000)
        }

        val categorySpec = ItemSpecification(SearchCriteria("category", category))
        val sellerSpec = ItemSpecification(SearchCriteria("seller", seller))
        val descriptionSpec = ItemSpecification(SearchCriteria("info", query.trim { it <= ' ' }))
        val fiSpec = ItemSpecification(SearchCriteria("firstInstance", "null"))
        val deletedSpec = ItemSpecification(SearchCriteria("deleted", deleted))
        val timeSpec = ItemSpecification(SearchCriteria("timestamp", startTime))

        return itemRepository.findAll(
                Specification
                        .where(categorySpec)
                        .and(sellerSpec)
                        .and(descriptionSpec)
                        .and(fiSpec)
                        .and(deletedSpec)
                        .and(timeSpec),
                PageRequest.of(page, count, orders))
    }

    @RequestMapping(value = ["/newitems"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun getNewItems(
            @RequestParam(required = true) start_time: Int,
            @RequestParam(required = false, defaultValue = "") query: String,
            @RequestParam(required = false, defaultValue = "ALL") category: Category,
            @RequestParam(required = false) seller: Seller?,
            @RequestParam(required = false, defaultValue = "false") deleted: Boolean): List<Item> {

        val categorySpec = ItemSpecification(SearchCriteria("category", category))
        val sellerSpec = ItemSpecification(SearchCriteria("seller", seller))
        val descriptionSpec = ItemSpecification(SearchCriteria("info", query.trim { it <= ' ' }))
        val fiSpec = ItemSpecification(SearchCriteria("firstInstance", "null"))
        val deletedSpec = ItemSpecification(SearchCriteria("deleted", deleted))
        val timeSpec = Specification<Item> { root, _, criteriaBuilder -> criteriaBuilder.greaterThan<Int>(root.get("timestamp"), start_time) }

        return itemRepository.findAll(
                Specification
                        .where(categorySpec)
                        .and(sellerSpec)
                        .and(descriptionSpec)
                        .and(fiSpec)
                        .and(deletedSpec)
                        .and(timeSpec))
    }


    @RequestMapping(value = ["/item"], method = [RequestMethod.GET], produces = ["application/json;charset=UTF-8"])
    @ResponseBody
    fun findById(@RequestParam id: Int): Any {
        val itemOptional = itemRepository.findById(id)

        return if (itemOptional.isPresent) {
            itemOptional.get()
        } else {
            Result.NOITEM
        }
    }
}