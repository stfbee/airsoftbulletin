package ru.ovm.aib.db.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import ru.ovm.aib.db.pojo.VkAlbum

/**
 * Created by vladislav
 * 18.12.2017
 * 15:09
 */
interface AlbumRepository : CrudRepository<VkAlbum, Int> {
    @Query("select count(e) from #{#entityName} e where e.deleted = false")
    fun countActive(): Long

    fun findAllByDeletedIs(deleted: Boolean): Iterable<VkAlbum>
}
