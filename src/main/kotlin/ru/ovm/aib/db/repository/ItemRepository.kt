package ru.ovm.aib.db.repository

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import ru.ovm.aib.db.pojo.Item
import ru.ovm.aib.db.pojo.Seller
import ru.ovm.aib.db.pojo.VkAlbum

/**
 * Created by vladislav
 * 18.12.2017
 * 16:41
 */
interface ItemRepository : PagingAndSortingRepository<Item, Int>, JpaSpecificationExecutor<Item> {
    fun findByUrl(url: String): Item?

    fun findByUrlAndDeletedIs(url: String, deleted: Boolean): Item?

    fun findBySellerOrderByTimestampAsc(seller: Seller): List<Item>

    fun findBySellerAndDeletedIsOrderByTimestampAsc(seller: Seller, deleted: Boolean): List<Item>

    fun findByVkAlbum(vkAlbum: VkAlbum): List<Item>

    fun findByVkAlbumAndDeletedIs(vkAlbum: VkAlbum, deleted: Boolean): List<Item>

    fun findAllByDeletedIs(deleted: Boolean): Iterable<Item>

    @Query("select count(e) from #{#entityName} e where e.deleted = false")
    fun countActive(): Long
}
