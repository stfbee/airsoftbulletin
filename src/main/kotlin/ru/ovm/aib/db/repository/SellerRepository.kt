package ru.ovm.aib.db.repository

import org.springframework.data.repository.CrudRepository
import ru.ovm.aib.db.pojo.Seller

/**
 * Created by vladislav
 * 18.12.2017
 * 16:41
 */
interface SellerRepository : CrudRepository<Seller, Int>
