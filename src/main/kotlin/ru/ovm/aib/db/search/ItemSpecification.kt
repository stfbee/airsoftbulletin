package ru.ovm.aib.db.search

import org.springframework.data.jpa.domain.Specification
import ru.ovm.aib.db.pojo.Category
import ru.ovm.aib.db.pojo.Item
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

/**
 * Created by vladislav
 * 29/08/2018
 * 04:08
 */
class ItemSpecification(private val criteria: SearchCriteria) : Specification<Item> {

    override fun toPredicate(root: Root<Item>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        val key = criteria.key
        val value = criteria.value

        val x = root.get<String>(key)

        //todo: use meta.Item_

        return when {
            "category".equals(key, ignoreCase = true) && Category.ALL != value -> builder.equal(x, value)
            "seller".equals(key, ignoreCase = true) && value != null -> builder.equal(x, value)
            "info".equals(key, ignoreCase = true) && !(value as String).isEmpty() -> builder.like(builder.upper(x), "%" + value.toUpperCase() + "%")
            "firstInstance".equals(key, ignoreCase = true) -> builder.isNull(x)
            "timestamp".equals(key, ignoreCase = true) -> builder.lessThanOrEqualTo(x, value.toString())
            "deleted".equals(key, ignoreCase = true) -> builder.equal(x, value)
            else -> null
        }
    }
}
