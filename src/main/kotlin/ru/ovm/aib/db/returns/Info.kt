package ru.ovm.aib.db.returns

import ru.ovm.aib.App

/**
 * Created by vladislav
 * 27.12.2017
 * 16:25
 */
data class Info(
        val items: Long,
        val groups: Long,
        val albums: Long,
        val version: String = App.version
)
