package ru.ovm.aib.db.pojo.meta

import ru.ovm.aib.db.pojo.*
import javax.persistence.metamodel.SetAttribute
import javax.persistence.metamodel.SingularAttribute
import javax.persistence.metamodel.StaticMetamodel


@StaticMetamodel(Item::class)
object Item_ {

    @Volatile
    lateinit var seller: SingularAttribute<Item, Seller>

    @Volatile
    lateinit var vkAlbum: SingularAttribute<Item, VkAlbum>

    @Volatile
    lateinit var category: SingularAttribute<Item, Category>

    @Volatile
    lateinit var photo_id: SingularAttribute<Item, Int>

    @Volatile
    lateinit var url: SingularAttribute<Item, String>

    @Volatile
    lateinit var photo_src: SingularAttribute<Item, String>

    @Volatile
    lateinit var description: SingularAttribute<Item, String>

    @Volatile
    lateinit var comments: SingularAttribute<Item, String>

    @Volatile
    lateinit var info: SingularAttribute<Item, String>

    @Volatile
    lateinit var timestamp: SingularAttribute<Item, Int>

    @Volatile
    lateinit var vkGroup: SingularAttribute<Item, VkGroup>

    @Volatile
    lateinit var duplicates: SetAttribute<Item, Item>

    @Volatile
    lateinit var firstInstance: SingularAttribute<Item, Item>

    @Volatile
    lateinit var deleted: SingularAttribute<Item, Boolean>
}