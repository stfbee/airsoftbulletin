package ru.ovm.aib.db.pojo

/**
 * Created by vladislav
 * 07.02.2018
 * 11:34
 */
enum class Status {
    UNKNOWN,
    FAIL,
    OK
}
