package ru.ovm.aib.db.pojo

import com.fasterxml.jackson.annotation.JsonBackReference

import javax.persistence.*

/**
 * Created by vladislav
 * 14.12.2017
 * 13:30
 */
@Entity
data class VkAlbum(
        @Id
        var vkId: Int = 0,

        var name: String,

        @Enumerated(EnumType.STRING)
        var category: Category,

        @JsonBackReference
        @ManyToOne(fetch = FetchType.EAGER)
        var vkGroup: VkGroup,

        @Enumerated(EnumType.STRING)
        var status: Status = Status.UNKNOWN,

        var deleted: Boolean = false)