package ru.ovm.aib.db.pojo

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.util.*
import javax.persistence.*

/**
 * Created by vladislav
 * 14.12.2017
 * 13:32
 */
@Entity
data class VkGroup(
        @Id
        var vkId: Int = 0,
        var name: String,

        @Enumerated(EnumType.STRING)
        var status: Status = Status.UNKNOWN,

        @JsonBackReference
        @JsonManagedReference
        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, mappedBy = "vkGroup")
        val albums: List<VkAlbum> = ArrayList(),

        var deleted: Boolean = false
)

