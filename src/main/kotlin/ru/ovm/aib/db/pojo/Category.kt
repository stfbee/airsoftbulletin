package ru.ovm.aib.db.pojo

/**
 * Created by vladislav
 * 14.12.2017
 * 13:32
 */
enum class Category {
    BLASTERS,
    GEAR,
    PARTS,
    MIXED,
    ALL
}
