package ru.ovm.aib.db.pojo

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

/**
 * Created by vladislav
 * 14.12.2017
 * 15:17
 */
@Entity
data class Seller(
        @Id
        @Column(unique = true)
        var vkId: Int = 0,
        var name: String,
        var city: String,
        var photoMax: String)

