package ru.ovm.aib.db.pojo

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonManagedReference
import ru.ovm.aib.Utils
import javax.persistence.*

@Entity
data class Item(

        @Id
        @GeneratedValue
        val id: Int = -1,

        @ManyToOne(fetch = FetchType.EAGER)
        var seller: Seller,

        @ManyToOne(fetch = FetchType.EAGER)
        var vkAlbum: VkAlbum,

        @Enumerated(EnumType.STRING)
        var category: Category,

        var photo_id: Int = 0,

        @Column(unique = true)
        var url: String = Utils.getPhotoLink(vkAlbum, photo_id),

        var photo_src: String,

        @get:JsonIgnore
        @Column(columnDefinition = "longtext")
        var description: String = "",

        @Column(columnDefinition = "longtext")
        @get:JsonIgnore
        var comments: String = "",

        @Column(columnDefinition = "longtext")
        var info: String = "",

        var timestamp: Int = 0,

        @ManyToOne(fetch = FetchType.LAZY)
        var vkGroup: VkGroup? = null,

        @JsonBackReference
        @JsonManagedReference
        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, mappedBy = "firstInstance")
        val duplicates: List<Item>? = null,

        @JsonBackReference
        @ManyToOne(fetch = FetchType.LAZY)
        @get:JsonIgnore
        var firstInstance: Item? = null,

        var deleted: Boolean = false) {

    fun addComment(comment: String) {
        this.comments += comment + "\n"
    }
}