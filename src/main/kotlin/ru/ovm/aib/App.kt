package ru.ovm.aib

import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.ServiceActor
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.httpclient.HttpTransportClient
import org.apache.http.impl.client.HttpClientBuilder
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling


@SpringBootApplication
@EnableScheduling
class App {
    companion object {
        val LOG = LoggerFactory.getLogger(App::class.java)

        lateinit var vk: VkApiClient
        lateinit var actorS: ServiceActor
        lateinit var actorU: UserActor

        var admin: String? = ""
        var group_id = 0
        var version = "2.1.0"

        /**
         * Минимальный размер описания, при котором он начинает считаться описанием.
         * Нужно, чтобы отсечь пустые описания или вроде "в первом комментарии" и тд
         */
        var min_desc_size = 25
    }
}

fun main(args: Array<String>) {

    val transportClient = HttpTransportClient.getInstance()
    try {
        val httpClientField = HttpTransportClient::class.java.getDeclaredField("httpClient")
        httpClientField.isAccessible = true
        httpClientField.set(null, HttpClientBuilder.create().useSystemProperties().build())
    } catch (e: NoSuchFieldException) {
        e.printStackTrace()
    } catch (e: IllegalAccessException) {
        e.printStackTrace()
    }

    App.vk = VkApiClient(transportClient)

    App.actorS = ServiceActor(6259405, System.getenv("token_s"))
    App.actorU = UserActor(8043960, System.getenv("token_u"))

    App.admin = System.getenv("admin_password") ?: ""

    val cover_group = System.getenv("cover_group")
    if (cover_group != null && !cover_group.isEmpty()) {
        App.group_id = Integer.valueOf(cover_group)!!
    }

    SpringApplication.run(App::class.java, *args)
}