package ru.ovm.aib

import org.springframework.core.io.ClassPathResource

import javax.imageio.ImageIO
import java.awt.*
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream

class VkHeaderGenerator {
    @Throws(IOException::class, FontFormatException::class)
    fun drawText(groups_count: String, albums_count: String, items_count: String, result_path: String) {

        val in_template = ClassPathResource("abin_header_template.png").inputStream
        val in_font = ClassPathResource("RobotoLight.ttf").inputStream

        val bufferedImage = ImageIO.read(in_template)
        val graphics = bufferedImage.createGraphics()
        graphics.color = Color.WHITE
        var font = Font.createFont(Font.TRUETYPE_FONT, in_font)
        font = font.deriveFont(Font.PLAIN, 60f)
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        graphics.font = font
        val fontMetrics = graphics.fontMetrics
        graphics.drawString(groups_count, 1301 - fontMetrics.stringWidth(groups_count), 133)
        graphics.drawString(albums_count, 1301 - fontMetrics.stringWidth(albums_count), 248)
        graphics.drawString(items_count, 1301 - fontMetrics.stringWidth(items_count), 363)
        ImageIO.write(bufferedImage, "png", File(result_path))
    }
}
