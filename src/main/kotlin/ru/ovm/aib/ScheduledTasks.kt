package ru.ovm.aib

import com.vk.api.sdk.exceptions.ApiException
import com.vk.api.sdk.exceptions.ClientException
import com.vk.api.sdk.objects.photos.Photo
import com.vk.api.sdk.queries.photos.PhotosGetCommentsSort
import com.vk.api.sdk.queries.users.UserField
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.ovm.aib.App.Companion.actorU
import ru.ovm.aib.App.Companion.vk
import ru.ovm.aib.db.pojo.Item
import ru.ovm.aib.db.pojo.Seller
import ru.ovm.aib.db.pojo.Status
import ru.ovm.aib.db.repository.AlbumRepository
import ru.ovm.aib.db.repository.GroupRepository
import ru.ovm.aib.db.repository.ItemRepository
import ru.ovm.aib.db.repository.SellerRepository
import java.awt.FontFormatException
import java.io.File
import java.io.IOException
import java.util.*


@Component
class ScheduledTasks @Autowired
constructor(private val albumRepository: AlbumRepository, private val itemRepository: ItemRepository, private val sellerRepository: SellerRepository, private val groupRepository: GroupRepository) {

    @Scheduled(fixedRate = (1000 * 60 * 15).toLong())
    @Throws(IOException::class, FontFormatException::class, ClientException::class, ApiException::class, InterruptedException::class)
    fun runWorkers() {
        runParser()
        checkOldItems()
        makeCover()

        LOG.info("Всё!")
    }

    @Throws(IOException::class, FontFormatException::class, ClientException::class, ApiException::class)
    fun makeCover() {
        if (App.group_id == 0) return

        val result_path = "image.png"
        VkHeaderGenerator().drawText(groupRepository.countActive().toString(), albumRepository.countActive().toString(), itemRepository.countActive().toString(), result_path)
        val ownerCoverPhotoUploadServer = vk.photos().getOwnerCoverPhotoUploadServer(actorU, App.group_id).cropX2(1590).cropY2(400).execute()
        val ownerCoverUploadResponse = vk.upload().photoOwnerCover(ownerCoverPhotoUploadServer.uploadUrl, File(result_path)).execute()
        vk.photos().saveOwnerCoverPhoto(actorU, ownerCoverUploadResponse.photo, ownerCoverUploadResponse.hash).execute()
    }

    @Throws(InterruptedException::class)
    fun runParser() {
        LOG.info("ЛЯ ШОБ СПАРСИТ")

        val start_count = itemRepository.countActive()
        LOG.info("В базе сейчас вот столько записей: $start_count")

        // Забираем список альбомов с комментами
        val vkAlbums = albumRepository.findAllByDeletedIs(false)
        // делаем по нему цикл
        for (vkAlbum in vkAlbums) {
            val albumVkId = vkAlbum.vkId.toLong()

            val vkGroup = vkAlbum.vkGroup
            LOG.info("Грузим альбом ${vkGroup.name}/${vkAlbum.name}  [https://vk.com/album-${vkGroup.vkId}_${vkAlbum.vkId}]")

            try {
                // для каждого альбома получаем все фотки
                val execute = vk
                        .photos()
                        .get(App.actorS)
                        .ownerId(-vkGroup.vkId)
                        .albumId(albumVkId.toString())
                        .rev(true)
                        .count(1000)
                        .extended(true)
                        .execute()

                val photos = execute.items

                // собираем список юзеров
                val user_ids = ArrayList<String>()
                for (photo in photos) {
                    // если userId==100 (сообщество), то скипаем цикл
                    val ownerId = photo.userId!!
                    if (ownerId == 100) continue
                    val id = ownerId.toString()
                    if (user_ids.contains(id)) continue
                    user_ids.add(id)
                }

                // грузим для юзеров ссылку на авку, имя и город
                val userObjects = vk
                        .users()
                        .get(App.actorS)
                        .userIds(user_ids)
                        .fields(UserField.CITY, UserField.PHOTO_MAX)
                        .execute()

                for (userObject in userObjects) {
                    val id = userObject.id

                    val byId = sellerRepository.findById(id)

                    if (byId.isPresent) {
                        val seller = byId.get()
                        seller.name = userObject.firstName + " " + userObject.lastName
                        if (userObject.city != null) {
                            seller.city = userObject.city.title
                        }
                        seller.photoMax = userObject.photoMax
                        sellerRepository.save(seller)
                    } else {
                        val seller = Seller(
                                id!!, userObject.firstName + " " + userObject.lastName,
                                if (userObject.city == null) "" else userObject.city.title,
                                userObject.photoMax)
                        sellerRepository.save(seller)
                    }
                }

                // для каждой фотки создаем лот с ссылкой на группу и продавца
                for (photo in photos) {
                    // если userId==100 (сообщество), то скипаем цикл
                    if (photo.userId == 100) continue

                    var item_in_db: Item? = itemRepository.findByUrl(Utils.getPhotoLink(vkAlbum, photo.id))

                    if (item_in_db == null) {
                        val sellerOptional = sellerRepository.findById(photo.userId)

                        if (!sellerOptional.isPresent) {
                            LOG.error("лол, селлер не загружен")
                            continue
                        }

                        item_in_db = Item(
                                seller = sellerOptional.get(),
                                vkAlbum = vkAlbum,
                                category = vkAlbum.category,
                                photo_id = photo.id,
                                photo_src = getMaxPhotoSize(photo),
                                description = photo.text,
                                timestamp = photo.date,
                                vkGroup = vkGroup
                        )

                        // проверяем описание и добавляем текст из комментов
                        addComments(item_in_db, photo)

                        // ищем дубликаты
                        val firstInstance = Utils.findDuplicate(itemRepository, item_in_db)

                        if (firstInstance != null) {
                            LOG.warn("Оп, найден дубликат!")
                            item_in_db.firstInstance = firstInstance
                        }

                        itemRepository.save(item_in_db)
                    } else {
                        if (item_in_db.deleted) {
                            item_in_db.deleted = false
                        }

                        item_in_db.description = photo.text

                        //на всякий случай тоже чекаем, вдруг описание в комментах появилось
                        addComments(item_in_db, photo)

                        itemRepository.save(item_in_db)
                    }
                }


                vkAlbum.status = Status.OK
                albumRepository.save(vkAlbum)
            } catch (e: ApiException) {
                e.printStackTrace()

                vkAlbum.status = Status.FAIL
                albumRepository.save(vkAlbum)
            } catch (e: ClientException) {
                e.printStackTrace()
                vkAlbum.status = Status.FAIL
                albumRepository.save(vkAlbum)
            }

        }


        val vkGroups = groupRepository.findAllByDeletedIs(false)
        for (vkGroup in vkGroups) {
            vkGroup.status = Status.UNKNOWN
            for (vkAlbum in vkGroup.albums) {
                if (vkAlbum.status == Status.OK && vkGroup.status != Status.FAIL) {
                    vkGroup.status = Status.OK
                } else if (vkAlbum.status == Status.FAIL) {
                    vkGroup.status = Status.FAIL
                    break
                }
            }
            groupRepository.save(vkGroup)
        }

        val end_count = itemRepository.countActive()
        LOG.info("В базу добавлено вот столько записей: " + (end_count - start_count) + ". Теперь в ней столько записей: " + end_count)
    }

    @Throws(InterruptedException::class, ClientException::class, ApiException::class)
    private fun addComments(item_in_db: Item, photo: Photo) {
        val description = item_in_db.description
        if (description.length < App.min_desc_size && item_in_db.comments.isEmpty()) {
            Thread.sleep(1000)
            // грузим комментарии к фотке
            val commentsResponse = App.vk.photos().getComments(App.actorU, photo.id!!).ownerId(photo.ownerId!!).sort(PhotosGetCommentsSort.ASC).execute()
            for (comment in commentsResponse.items) {
                // добавляем все первые комменты автора
                if (comment.fromId != photo.userId) break
                item_in_db.addComment(comment.text)
            }
        }

        val comments = item_in_db.comments
        if (comments.trim { it <= ' ' }.length > App.min_desc_size) {
            item_in_db.info = comments
        } else {
            item_in_db.info = description
        }

    }

    fun checkOldItems() {
        // TODO: 10.02.2018 check sellers

        LOG.info("ЛЯ ШОБ УДАЛИТ")

        // собираем все айдишники из бд в один список
        val photo_ids = ArrayList<String>()
        for (item in itemRepository.findAllByDeletedIs(false)) {
            photo_ids.add((-item.vkAlbum.vkGroup.vkId).toString() + "_" + item.photo_id)
        }

        val copy = ArrayList(photo_ids)
        val step = 300

        val times = Math.ceil((photo_ids.size / step.toFloat()).toDouble()).toInt()
        for (i in 0 until times) {

            val start_index = i * step
            val end_index = Math.min(start_index + step, photo_ids.size)


            val operation_photos = photo_ids.subList(start_index, end_index)
            try {
                // грузим фотки для этих айдишников
                val photos = vk
                        .photos()
                        .getById(actorU, operation_photos)
                        .execute()

                // если фотка загрузилась, то убираем ее айдишник из списка
                for (photo in photos) {
                    copy.remove(photo.ownerId.toString() + "_" + photo.id)
                }

                Thread.sleep(1000)
            } catch (e: ClientException) {
                e.printStackTrace()

                // в списке на удаление остаются только те эелементы, проверка которых упала с ошибкой 200 (нет доступа или удален)
                if (e !is ApiException || e.code != 200) {
                    copy.removeAll(operation_photos)
                }
            } catch (e: InterruptedException) {
                e.printStackTrace()
                if (e !is ApiException || e.code != 200) {
                    copy.removeAll(operation_photos)
                }
            } catch (e: ApiException) {
                e.printStackTrace()
                if (e.code != 200) {
                    copy.removeAll(operation_photos)
                }
            }

        }

        LOG.info("Найдено устаревших лотов: ${copy.size}. Удаляем...")

        // в итоге у нас остается список устаревших айдишников
        // для каждого их них получаем численный айди и "удаляем" его из бд
        for (photo_id in copy) {
            val item = itemRepository.findByUrlAndDeletedIs("photo$photo_id", false)
            item?.let {
                it.deleted = true
                itemRepository.save(it)
            }
        }
        LOG.info("Готово. В базе осталось записей: ${itemRepository.countActive()}")
    }


    private fun getMaxPhotoSize(photo: Photo): String {
        if (photo.photo2560 != null) return photo.photo2560
        if (photo.photo1280 != null) return photo.photo1280
        if (photo.photo807 != null) return photo.photo807
        if (photo.photo604 != null) return photo.photo604
        if (photo.photo130 != null) return photo.photo130
        return if (photo.photo75 != null) photo.photo75 else ""
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ScheduledTasks::class.java)
    }
}